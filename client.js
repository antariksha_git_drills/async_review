import fetch from 'node-fetch';

async function main() {
  const response = await fetch("http://localhost:3000/todos?user_id=1");
  const data = await response.json();
  //console.log(data);
  let fetchData=[];
  let allTodos={};
  //const delays=[delay1,delay2,delay3];
  //let delay1=[1,2,3,4,5];
  //let delay2=[6,7,8,9,10];
  //let delay3=[11,12,13,14,15];
  for(let i=1;i<16;i++){
    if(i==5||i==10){
      await delay(1000);
      console.log('delay');
    }
      fetchData=await fetchTodos(i)
      //console.log(fetchData,'line 11');
      allTodos[i]=fetchData['todos'];
    }
    //console.log(allTodos);
    let completion=[{id:0,name:'',numsToDosCompleted:0}]
    let index=0;
    //console.log(allTodos['14']);
    for(let key in allTodos){
      //console.log(allTodos[key]);
      let id=key;
      let name=`User ${key};`
      let numsToDosCompleted=0;
      for(let key2 of allTodos[key]){
        if(key2['isCompleted']){
          numsToDosCompleted++;
        }
      }
      
      completion[index]={id,name,numsToDosCompleted}
      index++;
    }
      console.log(completion);
    }

 function delay(ms){
  return new Promise(resolve => setTimeout(resolve, ms));
 }
async function fetchTodos(i){
    const response=await fetch(`http://localhost:3000/todos?user_id=${i}`)
    const data=await response.json();
    return data;
  }


main();


// write your code here
